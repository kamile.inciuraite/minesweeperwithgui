package minesweeperwitgui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class GameController implements Initializable {

    @FXML
    RadioButton cbCheck;
    @FXML
    private RadioButton cbPutAFlag;
    @FXML
    ImageView ivFace;
    @FXML
    private GridPane gpMap;
    Coordinate coordinate = null;
    String action = "check";
    static String[][] mapWithValues = new String[][]{
        {"1", "1", "1", "0", "0", "1", "m", "m", "2", "1"},
        {"2", "m", "2", "0", "1", "2", "3", "2", "2", "m"},
        {"3", "m", "3", "0", "1", "m", "1", "0", "1", "1"},
        {"3", "m", "2", "0", "1", "1", "1", "0", "0", "0"},
        {"m", "2", "1", "0", "1", "1", "2", "1", "1", "0"},
        {"1", "1", "1", "1", "2", "m", "2", "m", "1", "0"},
        {"0", "0", "1", "m", "4", "3", "3", "1", "1", "0"},
        {"0", "0", "2", "3", "m", "m", "1", "0", "0", "0"},
        {"0", "1", "2", "m", "3", "2", "1", "0", "0", "0"},
        {"0", "1", "m", "2", "1", "0", "0", "0", "0", "0"}
    };

    static String[][] mapForPlayer = new String[][]{
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"},
        {"@", "@", "@", "@", "@", "@", "@", "@", "@", "@"}
    };
    static int numberOfMines = getSymbolsInTheMap(mapWithValues, "m");

    public void exit(ActionEvent a1) {
        Platform.exit();
    }

    public void fillGridPane() throws FileNotFoundException {

        for (int row = 0; row < 10; row++) {
            for (int column = 0; column < 10; column++) {
                ImageView imageView = null;
                FileInputStream input = null;
                if (mapForPlayer[column][row].equals("@") || mapForPlayer[column][row].equals("flag")) {
                    String buttonId = "b" + Integer.toString(column) + Integer.toString(row);
                    Button button = new Button();
                    button.setId(buttonId);
                    if (mapForPlayer[column][row].equals("@")) {
                        button.setText("    ");
                    } else {
                        input = new FileInputStream("images/resized/flag.png");
                        Image image = new Image(input);
                        imageView = new ImageView(image);
                        button.setGraphic(imageView);
                    }
                    button.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            int row = Integer.parseInt(Character.toString(button.getId().charAt(1)));
                            int column = Integer.parseInt(Character.toString(button.getId().charAt(2)));
                            coordinate = new Coordinate(row, column);

                            try {
                                checkAfterCheckAction(coordinate);
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                fillGridPane();
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    gpMap.add(button, row, column);
                } 
                else {

                    if (mapForPlayer[column][row].equals("0")) {
                        input = new FileInputStream("images/resized/empty.png");
                    } else if (mapForPlayer[column][row].equals("1")) {
                        input = new FileInputStream("images/resized/1.png");
                    } else if (mapForPlayer[column][row].equals("2")) {
                        input = new FileInputStream("images/resized/2.png");

                    } else if (mapForPlayer[column][row].equals("3")) {
                        input = new FileInputStream("images/resized/3.png");

                    } else if (mapForPlayer[column][row].equals("4")) {
                        input = new FileInputStream("images/resized/4.png");

                    } else if (mapForPlayer[column][row].equals("5")) {
                        input = new FileInputStream("images/resized/5.png");
                    } else if (mapForPlayer[column][row].equals("")) {
                        input = new FileInputStream("images/resized/6.png");
                    } else if (mapForPlayer[column][row].equals("7")) {
                        input = new FileInputStream("images/resized/7.png");
                    } else if (mapForPlayer[column][row].equals("8")) {
                        input = new FileInputStream("images/resized/8.png");
                    } else if (mapForPlayer[column][row].equals("m")) {
                        input = new FileInputStream("images/resized/mine.png");
                    } else {
                        input = new FileInputStream("images/resized/redmine.png");
                    }
                    Image image = new Image(input);
                    imageView = new ImageView(image);
                    gpMap.add(imageView, row, column);
                }
            }
        }
    }

    public void endGame(Coordinate coordinate) throws FileNotFoundException {
        FileInputStream input = new FileInputStream("images/faces/gameover.png");
        Image image = new Image(input);
        ivFace.setImage(image);
        mapForPlayer = mapWithValues;
        fillGridPane();
    }

    public void winGame() throws FileNotFoundException {
        FileInputStream input = new FileInputStream("images/faces/winner.png");
        Image image = new Image(input);
        ivFace.setImage(image);
        mapForPlayer = mapWithValues;
        fillGridPane();
    }

    public void checkBoxCheckSelected(ActionEvent a6) {
        cbCheck.setSelected(true);
        cbPutAFlag.setSelected(false);
        action = "check";

    }

    public void checkBoxPutAFlagSelected(ActionEvent a7) {
        cbPutAFlag.setSelected(true);
        cbCheck.setSelected(false);
        action = "flag";
    }

    public void checkAfterCheckAction(Coordinate coordinate) throws FileNotFoundException {
        if (mapWithValues[coordinate.getRow()][coordinate.getColumn()].equals("m") && action.equals("check")) {
            mapWithValues[coordinate.getRow()][coordinate.getColumn()] = "M";
            endGame(coordinate);
        } else if (action.equals("flag")) {
            if (mapForPlayer[coordinate.getRow()][coordinate.getColumn()].equals("flag")) {
                mapForPlayer[coordinate.getRow()][coordinate.getColumn()] = "@";
            } else {
                mapForPlayer[coordinate.getRow()][coordinate.getColumn()] = "flag";
            }
        } else if (action.equals("check") && mapWithValues[coordinate.getRow()][coordinate.getColumn()].equals("0")) {

            checkAllNeighbourCells(coordinate, mapWithValues, mapForPlayer);

        } else {
            mapForPlayer[coordinate.getRow()][coordinate.getColumn()] = mapWithValues[coordinate.getRow()][coordinate.getColumn()];
        }

        int numberOfNotPressed = getSymbolsInTheMap(mapForPlayer, "@") + getSymbolsInTheMap(mapForPlayer, "flag");

        if (numberOfMines == numberOfNotPressed) {
            winGame();
        }
    }

    public static int getSymbolsInTheMap(String[][] map, String symbol) {
        int numberOfSymbols = 0;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                if (map[i][j].equals(symbol)) {
                    numberOfSymbols++;
                }
            }
        }
        return numberOfSymbols;
    }

    public static void checkAllNeighbourCells(Coordinate coordinate, String[][] mapWithValues, String[][] mapForPlayer) {
        try {
            ArrayList<Coordinate> listOfCoordinates = new ArrayList<>();
            listOfCoordinates.add(coordinate);
            for (int i = 0; i < listOfCoordinates.size(); i++) {
                coordinate.setRow(listOfCoordinates.get(i).getRow());
                coordinate.setColumn(listOfCoordinates.get(i).getColumn());
                int[][] directions = new int[][]{
                    {-1, 0},
                    {1, 0},
                    {-1, 1},
                    {0, 1},
                    {1, 1},
                    {-1, -1},
                    {0, -1},
                    {1, -1}
                };

                for (int j = 0; j < directions.length; j++) {
                    Coordinate coordinateForCheck = new Coordinate(coordinate.getRow() + directions[j][0], coordinate.getColumn() + directions[j][1]);

                    if (coordinateForCheck.getRow() >= 0 && coordinateForCheck.getColumn() >= 0
                            && coordinateForCheck.getRow() < mapWithValues.length
                            && coordinateForCheck.getColumn() < mapWithValues.length) {
                        if (mapWithValues[coordinateForCheck.getRow()][coordinateForCheck.getColumn()].equals("0")) {
                            Coordinate result = new Coordinate(coordinateForCheck.getRow(), coordinateForCheck.getColumn());

                            if (isInList(listOfCoordinates, result) == false) {
                                listOfCoordinates.add(result);
                            }

                            mapForPlayer[coordinateForCheck.getRow()][coordinateForCheck.getColumn()] = mapWithValues[coordinateForCheck.getRow()][coordinateForCheck.getColumn()];

                        } else {
                            mapForPlayer[coordinateForCheck.getRow()][coordinateForCheck.getColumn()] = mapWithValues[coordinateForCheck.getRow()][coordinateForCheck.getColumn()];
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isInList(ArrayList<Coordinate> listOfCoordinates, Coordinate mightBeInTheList) {
        for (Coordinate coordinate : listOfCoordinates) {
            if (coordinate.equals(mightBeInTheList)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
    }

}
