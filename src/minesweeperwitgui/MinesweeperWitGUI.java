package minesweeperwitgui;

import java.io.FileInputStream;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
/**
 * Kamilė Inčiūraitė
 * PRIF-17/4
 * 2019-11-11
 */
public class MinesweeperWitGUI extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader load = new FXMLLoader(getClass().getResource("start.fxml"));
        Parent root = load.load();
        StartController startController = load.getController();
        FileInputStream input = new FileInputStream("images/faces/smileyface.png");  
        Image image = new Image(input);
        startController.ivImage.setImage(image);
        Scene scene = new Scene(root);  
        primaryStage.setTitle("Minesweeper - menu");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
