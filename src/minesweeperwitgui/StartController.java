package minesweeperwitgui;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class StartController implements Initializable {
    @FXML
     ImageView ivImage;
    public void exit(ActionEvent a1)
    {
        Platform.exit();
    }
    public void startTheGame(ActionEvent a2) throws IOException
    {
        FXMLLoader load = new FXMLLoader(getClass().getResource("game.fxml"));
        Parent root = load.load();       
        GameController gameController = load.getController();
        gameController.fillGridPane();
        FileInputStream input = new FileInputStream("images/faces/smileyface.png");  
        Image image = new Image(input);
        gameController.ivFace.setImage(image);
        Scene scene = new Scene(root);
        Stage gameStage = new Stage();        
        gameStage.setTitle("Minesweeper - the game");
        gameStage.setScene(scene);
        gameStage.show();
        Stage menuStage = (Stage) ivImage.getScene().getWindow();
        menuStage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
      
    }
}
