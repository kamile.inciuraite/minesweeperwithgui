package minesweeperwitgui;


public class Coordinate {
    int row;
    int column;
    public Coordinate(int row, int column){
        this.row = row;
        this.column = column; 
    }

    @Override
    public boolean equals(Object obj) {
        Coordinate c = (Coordinate) obj;
        if (this.getRow() == c.getRow() && this.getColumn() == c.getColumn() ) {
                return true;
            }
        return false;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return "Coordinate{" + "row=" + row + ", column=" + column + '}';
    }
}
